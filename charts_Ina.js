window.onload = function () {
	// 1-AS CHART'AS
    var chart1 = new CanvasJS.Chart("chart1",
  {
    title:{
      text: "Derlingiausi metai",
    },
    exportEnabled: true,
    axisY: {
      includeZero:false,
      title: "Metai",
      interval: 10,
    }, 
    axisX: {
      interval:10,
      title: "Kaimai",
    },
    data: [
    {
      type: "rangeBar",
      showInLegend: true,
      yValueFormatString: "#0",
      indexLabel: "{y[#index]}",
      legendText: "Periodas (nuo/iki)",
      dataPoints: [   // Y: [Low, High]
        {x: 10, y:[1979, 1980], label: "Purnuškės"},
        {x: 20, y:[1968, 1970], label: "Grybai" },
        {x: 30, y:[1981, 1982], label: "Venecija" },
        {x: 40, y:[1990, 1995], label: "Zuikos"},
        {x: 50, y:[1972, 1980], label: "Petruškos"}
      ]
    }
    ]
  });
  chart1.render();

	// 2-AS CHART'AS
    var chart2 = new CanvasJS.Chart("chart2",
    {
      title:{
      text: "Gyventojų pasiskirstymas pagal lytį"   
      },
      animationEnabled: true,
      axisX:{
        title: "Kaimai"
      },
      axisY:{
        title: "Procentai %"
      },
      data: [
      {        
        type: "stackedColumn100",
        name: "Vyrai",
        showInLegend: "true",
        dataPoints: [
        {  y: 50, label: "Purnuškės"},
        {  y: 10, label: "Grybai" },
        {  y: 50, label: "Venecija" },
        {  y: 90, label: "Zuikos" },
        {  y: 21, label: "Petruškos"}                
        ]
      }, {        
        type: "stackedColumn100",        
        name: "Moterys",
        showInLegend: "true",
        dataPoints: [
        {  y: 20, label: "Purnuškės"},
        {  y: 20, label: "Grybai" },
        {  y: 50, label: "Venecija" },
        {  y: 10, label: "Zuikos" },
        {  y: 10, label: "Petruškos"}                
        ]
      }
      ]
    });
    chart2.render();
}