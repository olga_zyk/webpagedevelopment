window.onload = function () {
	// 1-AS CHART'AS
    var chart1 = new CanvasJS.Chart("chart1",
    {
      title:{
        text: "Užsienio turistų srautas 2016 metais"
      },
      data: [
      {
       type: "doughnut",
       dataPoints: [
       {  y: 30, indexLabel: "Vokietija" },
       {  y: 25, indexLabel: "Latvija" },
       {  y: 15, indexLabel: "Estija" },
       {  y: 15, indexLabel: "USA" },
       {  y: 15, indexLabel: "Others" }
       ]
     }
     ]
   	});
    chart1.render();

	// 2-AS CHART'AS
    var chart2 = new CanvasJS.Chart("chart2",
    {
      title:{
        text: "Lankytojų skaičius restorane"
      },
      data: [

      {
        dataPoints: [
        { x: 10, y: 195, label: "Ketvirtadienis"},
        { x: 20, y: 259,  label: "Penktadienis" },
        { x: 30, y: 301,  label: "Šeštadienis"},
        { x: 40, y: 122,  label: "Sekmadienis"},
        ]
      }
      ]
    });
    chart2.render();
}