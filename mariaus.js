
var marker;
var map;
var activeID = 0;

$(function(){
    $('#getID').click(function(){
           alert($(".active").attr('id'));
        });
    });

//aray'jai

var vardas = ["Ina", "Eimantas", "Marius", "Olga"];
var kaimas = ["RudamINA", "EIMANčiai", "MARIjampolis", "OLGAva"];
var mobilusis = ["aiphonis","samsungas","kyslotnas","baltas"];
var emailas = ["nesakysiu","eimantasau@gmail.com","mpleskys@gmail.com","me.me@somedomain.com"]; 
var zodiakas = ["&#9801","&#9809","&#9803","&#9804",]; //https://www.w3schools.com/charsets/ref_utf_symbols.asp
var kirciavimas = ["Rudaminà","Eimañčiai","Marijámpolis","Olgavà"];
var morze = [".-. ..- -.. .- -- .. -. .-",". .. -- .- -. -.-. .. .- ..","-- .- .-. .. .--- .- -- .--. --- .-.. .. ...","--- .-.. --. .- ...- .-"]
var latitude = ["54.5919", "55.631", "54.5324", "55.2906"];
var longitude = ["25.348", "23.0919", "25.3254", "23.3379"];
var linkas = ["personInfo_Ina.html", "personInfo_Eimantas.html", "personInfo_Marius.html", "personInfo_Olga.html"];

//uzkrauna card'o ir map'o data pagal active tab'a

	$(document).ready(function(){

           var activeTab =($(".active").attr('id'));
		   var tabID = ["infoI", "infoE", "infoM", "infoO"]
		   activeID = tabID.indexOf(activeTab);
		   changeMarkerPosition(activeID);
		   document.getElementById("vardas").innerHTML = vardas[activeID];
		   document.getElementById("kaimas").innerHTML = kaimas[activeID];
		   document.getElementById("mobilusis").innerHTML = mobilusis[activeID];
		   document.getElementById("emailas").innerHTML = emailas[activeID];
		   document.getElementById("zodiakas").innerHTML = zodiakas[activeID];
		   document.getElementById("kirciavimas").innerHTML = kirciavimas[activeID];
		   document.getElementById("morze").innerHTML = morze[activeID];
		   $("#linkas").attr("href", linkas[activeID]);
    })

//keicia card'o ir map'o data pagal active tab'a

$(function(){
    $('.tabs').click(function(){

           var activeTab =($(".active").attr('id'));
		   var tabID = ["infoI", "infoE", "infoM", "infoO"]
		   activeID = tabID.indexOf(activeTab);
		   changeMarkerPosition(activeID);
		   document.getElementById("vardas").innerHTML = vardas[activeID];
		   document.getElementById("kaimas").innerHTML = kaimas[activeID];
		   document.getElementById("mobilusis").innerHTML = mobilusis[activeID];
		   document.getElementById("emailas").innerHTML = emailas[activeID];
		   document.getElementById("zodiakas").innerHTML = zodiakas[activeID];
		   document.getElementById("kirciavimas").innerHTML = kirciavimas[activeID];
		   document.getElementById("morze").innerHTML = morze[activeID];
		   $("#linkas").attr("href", linkas[activeID]);

        });
});

//mapo draftas
	
function myMap() {
  var mapCanvas = document.getElementById("map");
  var mapOptions = {
		center: new google.maps.LatLng(latitude[0], longitude[0]), zoom: 15
	};
  map = new google.maps.Map(mapCanvas, mapOptions);
  marker = new google.maps.Marker({
		position: {lat: latitude[0], lng: longitude[0] },
		map: map
	});
} 

//mapo pakeitimas (ir marker'io) pagal active tab'a
	
function changeMarkerPosition(index) {
    var latlng = new google.maps.LatLng(latitude[index], longitude[index]);
    map.setCenter(latlng);
    marker.setPosition(latlng);
    
}
// chartu uzkrovimas
window.onload = function () {
		
// 1-AS CHART'AS
		
CanvasJS.addColorSet("mariaus",
        [
        "#00796B",
        "#009688",
		"#4CAF50",
        "#B2DFDB",          
        ]);
            
		var chart1 = new CanvasJS.Chart("chart1",
			{
			colorSet: "mariaus",
          
			theme: "theme4",
                        animationEnabled: true,
						backgroundColor: "#B2DFDB",
			title:{
              text: "Google paieškos Lietuvoje: 'Kates' vs 'Sunys' vs 'Alus'",
				fontSize: 24,
				fontColor: "#00796B",
				fontWeight: "bold",
			},
			toolTip: {
				shared: true
			},			
			axisY: {
				title: "Santykinis indeksas",
				titleFontColor: "#4CAF50",
				labelFontColor: "#4CAF50",
				lineThickness: 0,
				interlacedColor: "#B2DFBF",
				gridThickness: 0,
				maximum: 100,
			},
						
			axisX: {
				labelFontColor: "#4CAF50",
				labelFontSize: 12,
				labelAutoFit: false,
				labelAngle: 30,
			},
			
			data: [ 
			{
				type: "column",	
				name: "'Kates' search index",
				legendText: "'Kates' search index",
				showInLegend: true, 
				dataPoints:[
				{label: "Vilniaus", y: 38},
				{label: "Kauno", y: 43},
				{label: "Klaipėdos", y: 42},
				{label: "Šiaulių", y: 50},
				{label: "Panevėžio", y: 50},
				{label: "Alytaus", y: 75},
				{label: "Marijampolės", y: 88},
				{label: "Telšių", y: 60},
				{label: "Utenos", y: 58},
				{label: "Tauragės", y: 65}
				]
			},
			{
				type: "column",	
				name: "'Sunys' search index",
				legendText: "'Sunys' search index",
              //axisYType: "secondary",
				showInLegend: true,
				dataPoints:[
				{label: "Vilniaus", y: 32},
				{label: "Kauno", y: 41},
				{label: "Klaipėdos", y: 39},
				{label: "Šiaulių", y: 50},
				{label: "Panevėžio", y: 43},
				{label: "Alytaus", y: 74},
				{label: "Marijampolės", y: 86},
				{label: "Telšių", y: 64},
				{label: "Utenos", y: 52},
                {label: "Tauragės", y: 100}
				]
			},
              
            {
				type: "column",	
				name: "'Alus' search index",
				legendText: "'Alus' search index",
				showInLegend: true,
				dataPoints:[
				{label: "Vilniaus", y: 50},
				{label: "Kauno", y: 41},
				{label: "Klaipėdos", y: 40},
				{label: "Šiaulių", y: 37},
				{label: "Panevėžio", y: 49},
				{label: "Alytaus", y: 60},
				{label: "Marijampolės", y: 49},
				{label: "Telšių", y: 44},
				{label: "Utenos", y: 82},
                {label: "Tauragės", y: 76}

				]
			}],
			
			legend:{
            cursor:"pointer",
            itemclick: function(e){
				if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
              	e.dataSeries.visible = false;
				}
				else {
                e.dataSeries.visible = true;
				}
            	chart.render();
			}
        },
    });

	chart1.render();


	// 2-AS CHART'AS
     var chart2 = new CanvasJS.Chart("chart2",
    {      
		colorSet: "mariaus",
      theme:"theme4",
      title:{
		fontSize: 24,
		fontColor: "#00796B",
		fontWeight: "bold",
        text: "O kad sietųsi su tema, toks tikėtina fake grafikas ne į temą"
      },
      animationEnabled: true,
	  backgroundColor: "#B2DFDB",
		axisY :{
			title: "Katukų laimės indeksas",
			includeZero: true,
			maximum: 100,
			valueFormatString: "0",
			suffix: "%",
			titleFontColor: "#4CAF50",
			labelFontColor: "#4CAF50",
			lineThickness: 0,
			tickLength: 0,
		interlacedColor: "#B2DFD0",
		},
		axisY2: {    
			title:  "Pagautos pelės",
			includeZero: true,
			maximum: 10,
			valueFormatString: "0",
			suffix: " pelės/d.",
			titleFontColor: "#4CAF50",
			labelFontColor: "#4CAF50",
			lineThickness: 0,
			tickLength: 0,
			interlacedColor: "#B2DFBF",
			},
		axisX: {
		labelFontColor: "#4CAF50",
		labelFontSize: 12,
		labelAutoFit: false,
		labelAngle: 30,
		},
      toolTip: {
        shared: "true"
		},
      data: [
      {        
        type: "spline", 
        showInLegend: true,
        name: "Katukų laimės indx",
        // markerSize: 0,        
        // color: "rgba(54,158,173,.6)",
        dataPoints: [
        {label: "Vilnius", y: 15},
        {label: "Kaunas", y: 25},
        {label: "Klaipėda", y: 40},
        {label: "Kiti miestai", y: 60},
        {label: "Miesteliai", y: 85},
        {label: "Kaimai", y: 99},
        {label: "Vienkiemiai", y: 90},
        {label: "Garfieldas", y: 66},  
        ]
      },
      {        
        type: "column", 
        axisYType: "secondary",
        showInLegend: true,
        // markerSize: 0,
        name: "Pagautos pelės",
        dataPoints: [
 		{label: "Vilnius", y: 1},
        {label: "Kaunas", y: 1.5},
        {label: "Klaipėda", y: 2},
        {label: "Kiti miestai", y: 3},
        {label: "Miesteliai", y: 5},
        {label: "Kaimai", y: 8},
        {label: "Vienkiemiai", y: 9},
        {label: "Garfieldas", y: 0.1},  
        ]
      }],
	  
      legend:{
        cursor:"pointer",
        itemclick : function(e) {
          if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible ){
          	e.dataSeries.visible = false;
          }
          else {
            e.dataSeries.visible = true;
          }
          chart.render();
        }
      },
    });

chart2.render();
}

